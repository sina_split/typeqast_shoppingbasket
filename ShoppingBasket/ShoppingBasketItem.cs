﻿namespace ShoppingBasket
{
	/// <summary>
	/// A "wrapper" for placing Products in the Basket
	/// </summary>
	public class ShoppingBasketItem
	{
		public int Quantity { get; private set; }
		private Product _product = null;

		private int _productId;
		public int ProductId
		{
			get { return _productId; }
			set
			{
				_productId = value;

				// To ensure that Product will be re-created
				_product = null;
			}
		}

		public Product Product
		{
			get
			{
				// Product got/created only when needed
				if (_product == null)
				{
					_product = new Product(ProductId);
				}

				return _product;
			}
		}

		public ShoppingBasketItem(int productId, int quantity)
		{
			ProductId = productId;
			Quantity = quantity;
		}

		public decimal Price
		{
			get { return Product.Price; }
		}
	}
}
