﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace ShoppingBasket
{
	public class ShoppingBasket : Component
	{
		private Dictionary<int, int> ShoppingBasketItems { get; set; }   // ProductId, Quantity
		private Dictionary<int, int> ShoppingBasketDiscounts { get; set; }   // ProductId, DiscountPercentage

		private List<ShoppingBasketItem> BasketContents { get; set; }
		private List<ShoppingBasketDiscount> BasketDiscounts { get; set; }
		private decimal SumPrice { get; set; }

		public static ShoppingBasket CreateShoppingBasket()
		{
			return new ShoppingBasket
			{
				ShoppingBasketItems = new Dictionary<int, int>(),
				ShoppingBasketDiscounts = new Dictionary<int, int>(),
				BasketContents = new List<ShoppingBasketItem>(),
				BasketDiscounts = new List<ShoppingBasketDiscount>()
			};
		}

		protected ShoppingBasket() {

		}

		/// <summary>
		/// Adds a quantity of a Product to the Basket  
		/// </summary>
		/// <param name="productId"></param>
		/// <param name="quantity"></param>
		public void AddItemToBasket(int productId, int quantity = 1)
		{
			if (!ShoppingBasketItems.ContainsKey(productId))	// If it's not in the Basket, add with Quntity of 0
			{
				ShoppingBasketItems.Add(productId, 0);
			}

			ShoppingBasketItems[productId] += quantity; // Update Quantity
		}

		/// <summary>
		/// Removes entire Product from the Basket, regardless of Quantity
		/// </summary>
		/// <param name="productId"></param>
		public void RemoveItemFromBasket(int productId)
		{
			ShoppingBasketItems.Remove(productId);
		}

		/// <summary>
		/// Gets the Quantity of a Product in the Basket
		/// </summary>
		/// <param name="productId"></param>
		/// <returns>0 if Product is not in the Basket</returns>
		public int GetItemQuantity(int productId)
		{
			if (ShoppingBasketItems.TryGetValue(productId, out int itemQuantity))   // If it's already in the Dictionary, just update Quantity
			{
				return itemQuantity;
			}

			// The Product was not added to the Basket
			return 0;
		}

		/// <summary>
		/// Calculates the total sum price of all Products in the Basket, counting in the possible Discounts
		/// </summary>
		/// <returns>Total Sum Price</returns>
		public decimal GetSumPrice()
		{
			GetAllDiscounts();

			CalculateSumPrice();

			// Log GetSumPrice Request
			LogSumPriceRequest();

			return SumPrice;
		}

		/// <summary>
		/// Determines all available discounts applicable to the current Basket contents
		/// </summary>
		/// <returns>Dictionary&lt;ProductId, DiscountPercentage&gt;</returns>
		private void GetAllDiscounts()
		{
			ShoppingBasketDiscounts = new Dictionary<int, int>();    // DiscountedProductId, PercentageDiscount

			List<ShoppingBasketDiscount> BasketDiscounts = new List<ShoppingBasketDiscount>();

			foreach (KeyValuePair<int, int> item in ShoppingBasketItems)
			{
				// Get discount if exists
				ShoppingBasketDiscount discount = new ShoppingBasketDiscount(item.Key, item.Value);

				if (!ShoppingBasketDiscounts.ContainsKey(discount.DiscountedProductId))
				{
					ShoppingBasketDiscounts.Add(discount.DiscountedProductId, 0);
				}

				ShoppingBasketDiscounts[discount.DiscountedProductId] += discount.TotalDiscount;

				BasketDiscounts.Add(discount);
			}
		}

		private void CalculateSumPrice()
		{
			SumPrice = 0m;

			// Dictionary of Products with Quantities
			foreach (KeyValuePair<int, int> item in ShoppingBasketItems)
			{
				// Create a new Item/Product to add to the cart
				ShoppingBasketItem newItem = new ShoppingBasketItem(item.Key, item.Value);  // Get the Product/Price

				BasketContents.Add(newItem);

				// Total price = Single Price * Quantity
				decimal totalItemPrice = newItem.Price * item.Value;

				// Get discount if applicable for this Product
				if (ShoppingBasketDiscounts.TryGetValue(item.Key, out int percentage))
				{
					totalItemPrice -= (decimal)percentage / 100 * newItem.Price;
				}

				SumPrice += totalItemPrice;
			}
		}

		/// <summary>
		/// Every time a total sum price of the basket is requested, it has to be logged for analytic purposes
		/// Showing details about all items in the basket, all applicable discounts, and a total sum price of the basket.
		/// </summary>
		private void LogSumPriceRequest()
		{
			// Use logging to console for the sake of simplicity.
			Console.WriteLine("LogSumPriceRequested: ");

			Console.WriteLine("All Items in the Basket: ");

			foreach (ShoppingBasketItem item in BasketContents)
			{
				Console.WriteLine(item.Quantity + " * " + item.Product.Name + ": $" + item.Quantity * item.Price);
			}

			Console.WriteLine("All applicable Discounts: ");

			foreach (ShoppingBasketDiscount discount in BasketDiscounts)
			{
				Console.WriteLine(discount.Description);
			}

			Console.WriteLine("Total Sum Price of the Basket: $" + SumPrice);
		}

		~ShoppingBasket()
		{ }
	}
}
