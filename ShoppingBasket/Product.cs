﻿namespace ShoppingBasket
{
	/// <summary>
	/// A "fake" Product class, instead of DB Model/Service or whatever
	/// </summary>
	public class Product
	{
		public int ProductId { get; set; }
		public decimal Price { get; set; }
		public string Name { get; set; }

		/// <summary>
		/// "Creates" a new Product
		/// </summary>
		/// <param name="productId"></param>
		public Product(int productId)
		{
			ProductId = productId;

			switch (productId)
			{
				case 1: // Butter
					Name = "Butter";
					Price = 0.80m;
					return;
				case 2: // Milk
					Name = "Milk";
					Price = 1.15m;
					break;
				case 3: // Bread
					Name = "Bread";
					Price = 1.00m;
					break;
			}
		}
	}
}
