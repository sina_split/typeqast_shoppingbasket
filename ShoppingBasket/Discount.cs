﻿namespace ShoppingBasket
{
	/// <summary>
	/// A "fake" discount class
	/// </summary>
	public class Discount
	{
		public int DiscountId { get; set; }
		public int ProductId { get; set; }
		private int NeededQuantity { get; set; }
		private int DiscountPercentage { get; set; }
		public int TotalDiscount { get; set; }
		public string Description { get; set; }

		/// <summary>
		/// Returns a percentage of discount on a Product
		/// Determined by having a certain Quantity of certain Product
		/// </summary>
		/// <param name="productId"></param>
		/// <param name="quantity"></param>
		public Discount(int productId, int quantity)
		{
			switch (productId)
			{
				case 1:		// Butter
					{
						// Buy 2 butters and get 1 bread at 50% off
						NeededQuantity = 2;
						ProductId = 3;              // Bread
						DiscountPercentage = 50;    // 50%
						Description = "Buy 2 butters and get 1 bread at 50% off";
						break;
					}
				case 2:		// Milk
					{
						// Buy 3 milks and get 1 milk for free
						NeededQuantity = 3;
						ProductId = 2;              // Milk
						DiscountPercentage = 100;   // 50%
						Description = "Buy 3 milks and get the 4th milk for free";
						break;
					}
				default:    // 
					{
						ProductId = productId;
						TotalDiscount = 0;
						return;
					}
			}

			int discountCount = quantity / NeededQuantity;

			TotalDiscount = discountCount * DiscountPercentage;
		}
	}
}
