﻿namespace ShoppingBasket
{
	public class ShoppingBasketDiscount
	{
		public int DiscountedProductId { get; set; }
		private int Quantity { get; set; }
		public int TotalDiscount { get; set; }
		public string Description { get; set; }

		private Discount _discount = null;

		private int _productId;
		private int ProductId
		{
			get { return _productId; }
			set
			{
				_productId = value;

				// To ensure that Product will be re-created
				_discount = null;
			}
		}

		public Discount Discount
		{
			get
			{
				// Discount got/created only when needed
				if (_discount == null)
				{
					_discount = new Discount(ProductId, Quantity);
				}

				return _discount;
			}
		}

		public ShoppingBasketDiscount(int productId, int quantity)
		{
			ProductId = productId;
			Quantity = quantity;

			DiscountedProductId = Discount.ProductId;
			TotalDiscount = Discount.TotalDiscount;
			Description = Discount.Description;
		}
	}
}
