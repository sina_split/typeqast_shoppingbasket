﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ComponentTest
{
	[TestClass]
	public class Test
	{
		private ShoppingBasket.ShoppingBasket _shoppingBasket;

		[TestInitialize]
		public void TestInitialize()
		{
			_shoppingBasket = ShoppingBasket.ShoppingBasket.CreateShoppingBasket();
		}

		[TestMethod]
		public void ShoppingBasket_ShouldAddItem()
		{
			_shoppingBasket.AddItemToBasket(1, 1);

			Assert.AreEqual(1, _shoppingBasket.GetItemQuantity(1));

			_shoppingBasket.AddItemToBasket(2, 2);

			Assert.AreEqual(2, _shoppingBasket.GetItemQuantity(2));

			_shoppingBasket.AddItemToBasket(3, 3);

			Assert.AreEqual(3, _shoppingBasket.GetItemQuantity(3));
		}

		[TestMethod]
		public void ShoppingBasket_ShouldUpdateItemQuantity()
		{
			_shoppingBasket.AddItemToBasket(1, 1);
			_shoppingBasket.AddItemToBasket(1, 2);

			Assert.AreEqual(3, _shoppingBasket.GetItemQuantity(1));

			_shoppingBasket.AddItemToBasket(2, 2);
			_shoppingBasket.AddItemToBasket(2, 3);

			Assert.AreEqual(5, _shoppingBasket.GetItemQuantity(2));

			_shoppingBasket.AddItemToBasket(3, 3);
			_shoppingBasket.AddItemToBasket(3, 4);

			Assert.AreEqual(7, _shoppingBasket.GetItemQuantity(3));
		}

		[TestMethod]
		public void ShoppingBasket_ShouldRemoveItem()
		{
			_shoppingBasket.AddItemToBasket(1, 1);
			_shoppingBasket.AddItemToBasket(2, 2);
			_shoppingBasket.AddItemToBasket(3, 3);

			_shoppingBasket.RemoveItemFromBasket(1);

			Assert.AreEqual(0, _shoppingBasket.GetItemQuantity(1));

			_shoppingBasket.RemoveItemFromBasket(2);

			Assert.AreEqual(0, _shoppingBasket.GetItemQuantity(2));

			_shoppingBasket.RemoveItemFromBasket(3);

			Assert.AreEqual(0, _shoppingBasket.GetItemQuantity(3));

			/*
			 * Scenarios:
			 * Given the basket has 1 bread, 1 butter and 1 milk, when I total the basket then the total should be $2.95
			 * Given the basket has 2 butters and 2 breads, when I total the basket then the total should be $3.10
			 * Given the basket has 4 milks, when I total the basket then the total should be $3.45
			 * Given the basket has 2 butter, 1 bread and 8 milk, when I total the basket then the total should be $9.00
			 */
		}

		[TestMethod]
		public void ShoppingBasket_ShouldCalculateSumPriceWithDiscounts1()
		{
			// Given the basket has 1 bread, 1 butter and 1 milk, when I total the basket then the total should be $2.95
			_shoppingBasket.AddItemToBasket(1, 1);
			_shoppingBasket.AddItemToBasket(2, 1);
			_shoppingBasket.AddItemToBasket(3, 1);

			Assert.AreEqual(2.95m, _shoppingBasket.GetSumPrice());
		}

		[TestMethod]
		public void ShoppingBasket_ShouldCalculateSumPriceWithDiscounts2()
		{
			//  Given the basket has 2 butters and 2 breads, when I total the basket then the total should be $3.10
			_shoppingBasket.AddItemToBasket(1, 2);
			_shoppingBasket.AddItemToBasket(3, 2);

			Assert.AreEqual(3.10m, _shoppingBasket.GetSumPrice());
		}

		[TestMethod]
		public void ShoppingBasket_ShouldCalculateSumPriceWithDiscounts3()
		{
			// Given the basket has 4 milks, when I total the basket then the total should be $3.45
			_shoppingBasket.AddItemToBasket(2, 4);

			Assert.AreEqual(3.45m, _shoppingBasket.GetSumPrice());
		}

		[TestMethod]
		public void ShoppingBasket_ShouldCalculateSumPriceWithDiscount4()
		{
			// Given the basket has 2 butter, 1 bread and 8 milk, when I total the basket then the total should be $9.00
			_shoppingBasket.AddItemToBasket(1, 2);
			_shoppingBasket.AddItemToBasket(2, 8);
			_shoppingBasket.AddItemToBasket(3, 1);

			Assert.AreEqual(9.00m, _shoppingBasket.GetSumPrice());
		}
	}
}
